﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class start : MonoBehaviour
{
    private void Awake()
    {
        EventTrigger.Entry pressDown = new EventTrigger.Entry();
        pressDown.eventID = EventTriggerType.PointerDown;
        pressDown.callback.AddListener((data) => { ShowLog(); });   //メソッド登録

        //イベントトリガーにイベント追加
        GetComponent<EventTrigger>().triggers.Add(pressDown);
    }

    //ログを出すだけのメソッド
    public void ShowLog()
    {
        Debug.Log("ログ！");
    }

// Start is called before the first frame update
void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Sbutton()
    {
        Destroy(gameObject);
        //gameObject.SetActive(false);
    }
}
