﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class splash : MonoBehaviour
{
    public GameObject Splashprefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject splash = Instantiate(Splashprefab);
        splash.transform.position = transform.position;
    }
}
