﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoad : MonoBehaviour
{
  
    public string nextstageName = "stage01";  // 次のシーンの名前をunity側で入れる
    private static int stageNm = 0;
    public string CleaStageNm;

    //リワード広告用
    GameObject Button;
    GameObject Water;
    GameObject Player; 

    static public Material newMat;

    static public bool WatchVideo = false;

    AudioSource audioSource;
    public void Start()
    {
        //newMat = TempMat;
        stageNm = PlayerPrefs.GetInt("StageNum", 0);
 

        Button = GameObject.Find("NextStageButton");
        //Hose = GameObject.Find("Particle _Hose");

        Player = GameObject.Find("Particle _Hose");

        audioSource = Player.GetComponent<AudioSource>();
    }

    //ボタンが押されたときの処理
    public void LoadStage()
    {
        gameObject.SetActive(false);


        //PlayerPrefs.SetString("SceneName", nextstageName);
        stageNm++;

        //stageNmのセーブ
        PlayerPrefs.SetInt("StageNum", stageNm);
        PlayerPrefs.Save();

        SceneManager.sceneLoaded += GameSceneLoaded;


        //Save
        PlayerPrefs.SetString("SceneName", "");
        PlayerPrefs.SetString("SceneName", nextstageName); //シーンのセーブ

        ////クリアしたステージの名前の取得
        //CleaStageNm = PlayerPrefs.GetString("ClearStageNm", SceneManager.GetActiveScene().name);
        ////クリアしたステージ数の報告
        //GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", CleaStageNm);
        ////今のステージの名前の保存
        //PlayerPrefs.SetString("ClearStageNm", CleaStageNm = SceneManager.GetActiveScene().name);

    }
    private void GameSceneLoaded(Scene next, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= GameSceneLoaded;
        var nm = GameObject.Find("Nm");
        if (nm == null)
        {
            return;
        }
        nm.GetComponent<Text>().text = stageNm.ToString(); //UIの書き換え
    }

    public void Advertisingdisplay()
    {
        //gameObject.SetActive(false);


        //PlayerPrefs.SetString("SceneName", nextstageName);
        stageNm++;

        //stageNmのセーブ
        PlayerPrefs.SetInt("StageNum", stageNm);
        PlayerPrefs.Save();

        SceneManager.sceneLoaded += GameSceneLoaded;


        //Save
        PlayerPrefs.SetString("SceneName", "");
        PlayerPrefs.SetString("SceneName", nextstageName); //シーンのセーブ

        ////クリアしたステージの名前の取得
        //CleaStageNm = PlayerPrefs.GetString("ClearStageNm", SceneManager.GetActiveScene().name);
        ////クリアしたステージ数の報告
        //GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", CleaStageNm);
        ////今のステージの名前の保存
        //PlayerPrefs.SetString("ClearStageNm", CleaStageNm = SceneManager.GetActiveScene().name);


        //////////広告の表示///////////////////////
        // ◆ShowRewardedVideoの結果を受け取るコールバックを設定
        System.Action onFinished = () =>
        {
            // ◆動画リワード広告が最後まで視聴された際の処理
            Debug.Log("上のほうだよ！！終わりだよ！！！！");
            LoadNextStage("Finished"); //動画広告が見られたらDashBoardにFinishedが帰る
        };
        System.Action onSkipped = () =>
        {
            // ◆動画リワード広告がスキップされた際の処理
            LoadNextStage("Skipped"); //動画広告が見られたらDashBoardにSkippedが帰る
        };
        System.Action onFailed = () =>
        {
            // ◆動画リワード広告が再生失敗した際の処理
            LoadNextStage("Failed");
        };
        // ◆1番の動画広告を表示
        GLS.Ad.ShowRewardedVideo(1, onFinished, onSkipped, onFailed);
        SceneManager.LoadScene(nextstageName);
    }

    //リワード広告
    public void Reward()
    {
        //gameObject.SetActive(false);


        //PlayerPrefs.SetString("SceneName", nextstageName);
        stageNm++;

        //stageNmのセーブ
        PlayerPrefs.SetInt("StageNum", stageNm);
        PlayerPrefs.Save();

        SceneManager.sceneLoaded += GameSceneLoaded;


        //Save
        PlayerPrefs.SetString("SceneName", "");
        PlayerPrefs.SetString("SceneName", nextstageName); //シーンのセーブ

        ////クリアしたステージの名前の取得
        //CleaStageNm = PlayerPrefs.GetString("ClearStageNm", SceneManager.GetActiveScene().name);
        ////クリアしたステージ数の報告
        //GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", CleaStageNm);
        ////今のステージの名前の保存
        //PlayerPrefs.SetString("ClearStageNm", CleaStageNm = SceneManager.GetActiveScene().name);


        ///////////広告の表示//////////////
        // ◆ShowRewardedVideoの結果を受け取るコールバックを設定
        System.Action onFinished = () =>
        {
            // ◆動画リワード広告が最後まで視聴された際の処理
            ClickReward("Finished"); //動画広告が見られたらDashBoardにFinishedが帰る
            
            Debug.Log("下のほうだよ！！終わりだよ！！！！");
        };
        System.Action onSkipped = () =>
        {
            // ◆動画リワード広告がスキップされた際の処理
            ClickReward("Skipped"); //動画広告が見られたらDashBoardにSkippedが帰る
        };
        System.Action onFailed = () =>
        {
            // ◆動画リワード広告が再生失敗した際の処理
            ClickReward("Failed");
        };
        // ◆0番の動画リワード広告を表示
        GLS.Ad.ShowRewardedVideo(0, onFinished, onSkipped, onFailed);
        SceneManager.LoadScene(nextstageName);
    }

   
 

    private void LoadNextStage(string adVideoResult)
    {
        Debug.Log("LoadNextStage");

        //広告を見たことを報告
        GLS.GLSAnalyticsUtility.TrackEvent("Ad Video", "Result", adVideoResult);
        // SceneManager.LoadScene(nextstageName);
    }

    private void ClickReward(string RwVideoResult)
    {
        GLS.GLSAnalyticsUtility.TrackEvent("Rw Video", "RwResult", RwVideoResult);
    }
    public void WatchReward()
    {
        Debug.Log("押してる");
        WatchVideo = true;
    }

   

    static public void ClearedStage()
    {
        if (stageNm==3)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", "Stage3");
        }
        else if(stageNm==6)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", "Stage6");
        }
        else if(stageNm==9)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", "Stage9");
        }
        else if(stageNm==12)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", "Stage12");
        }
        else if(stageNm==15)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", "Stage15");
        }
        else if(stageNm==18)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", "Stage18");
        }
        else if(stageNm==21)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("Report of the cleared stage", "cleared stage", "Stage21");
        }
    }
}