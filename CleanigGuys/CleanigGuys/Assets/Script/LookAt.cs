﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    public GameObject targetObject; // 注視したいオブジェクトを事前にInspectorから入れておく
   
    void Update()
    {
        this.transform.LookAt(targetObject.transform);
    }
    
}
