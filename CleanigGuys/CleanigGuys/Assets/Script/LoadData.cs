﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadData : MonoBehaviour
{
    void Start()
    {
        ////Load
        string nextstageName = PlayerPrefs.GetString("SceneName", "stage00"); //シーンのロード
        SceneManager.LoadScene(nextstageName);
    }
}
