﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CleanScript : MonoBehaviour
{
    public Material[] _material;
    public int Hit = 0;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public void OnCollisionEnter(Collision collision)

    {
        if (collision.gameObject.tag == "Shell") //もしタグ"Shell"がぶつかったら
        {
            Hit++;
            Debug.Log(Hit);

            if (Hit >= 2)
            {
                GetComponent<Renderer>().sharedMaterial = _material[1];　//マテリアル1を割り当てる
            }

            if (Hit >=5)
            {
                GetComponent<Renderer>().sharedMaterial = _material[2]; //マテリアル2を割り当てる
                Destroy(gameObject);



                GameObject director = GameObject.Find("GameDirector");
                director.GetComponent<GameDirector>().Thankyou();
            }                                                                 

        }
    }
}
