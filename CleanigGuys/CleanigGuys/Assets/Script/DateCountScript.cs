﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DateCountScript : MonoBehaviour
{
    public static int logInDaysCounter = 0;
    public static string logInDaysCountKey = "LOGINDAYS";

    public static float stayTime;       //GameDirectorで使います


    public static void AddLogInDays()
    {
        logInDaysCounter = PlayerPrefs.GetInt(logInDaysCountKey, 0);

        DateTime now = DateTime.Now;
        int todayInt = 0;

        todayInt = now.Year * 1000 + now.Month * 100 + now.Day;

        if (!PlayerPrefs.HasKey("Date"))
        {
            Debug.Log("Dateというデータが存在しません");
            PlayerPrefs.SetInt("Date", todayInt);
        }
        else
        {
            if (todayInt - PlayerPrefs.GetInt("Date") > 0)
            {
                PlayerPrefs.SetInt("Date", todayInt);
                Debug.Log("次の日になりました");
                logInDaysCounter++;
                PlayerPrefs.SetInt(logInDaysCountKey, logInDaysCounter);
                GLS.GLSAnalyticsUtility.TrackEvent("Login days report", "Login days", logInDaysCounter);
            }
            else
            {
                Debug.Log("今日はすでにログインしています");
            }
        }
    }
}
