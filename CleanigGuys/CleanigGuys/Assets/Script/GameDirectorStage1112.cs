﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirectorStage1112 : MonoBehaviour
{
    GameObject Wash;
    GameObject Amount;
    GameObject RewardUI;

    public static int wash = 0;
    float OnStageTime = 0;


    //[SerializeField]
    ////　ポーズした時に表示するUIのプレハブ
    //public GameObject RewardUIPrefab;
    ////　ポーズUIのインスタンス
    //private GameObject RewardUIInstance;


    public static int thankyou = 0;

    //一度だけ送信したいので
    private bool oneTime60sec = true;
    private bool oneTime180sec = true;
    private bool oneTime300sec = true;

    //洗う数
    public int WashAmount;

    public float X;
    public float Y;
    public float Z;
    // Start is called before the first frame update
    void Start()
    {
        thankyou = 0;

        //洗った数のテキスト
        Wash = GameObject.Find("WashText");

        //ゲームクリア時のUI
        RewardUI = GameObject.Find("RewardUI");

        RewardUI.gameObject.SetActive(false);

        //SceneLoad.WatchVideo = false;

        ////洗う数のランダム化
        //WashAmount = Random.Range(3, 9);            //3～9体のおっさんを生み出す
        Amount = GameObject.Find("AmountText");

        //きれいにする人の数
        Amount.GetComponent<Text>().text = WashAmount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        OnStageTime += Time.deltaTime;

        //ここに起動してからの時間が蓄積されていく
        DateCountScript.stayTime += Time.deltaTime;


        //一定時間に達したら一度だけカスタムイベントを送信
        if (DateCountScript.stayTime > 60f)
        {
            if (oneTime60sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("SendStayTimeReport", "StayTime60sec", DateCountScript.stayTime);
                oneTime60sec = false;
            }
        }
        if (DateCountScript.stayTime > 180f)
        {
            if (oneTime180sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("SendStayTimeReport", "StayTime180sec", DateCountScript.stayTime);
                oneTime180sec = false;
            }
        }
        if (DateCountScript.stayTime > 300f)
        {
            if (oneTime300sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("SendStayTimeReport", "StayTime300sec", DateCountScript.stayTime);
                oneTime300sec = false;
            }
        }
    }

    public void Clean()
    {
        wash++;
        Wash.GetComponent<Text>().text = wash.ToString(); //きれいにした人数の表示
    }

    public void Thankyou()
    {
        thankyou++;
        Debug.Log(thankyou);



        //全員きれいにしたら
        if (thankyou == WashAmount)
        {
            //SceneLoadのWatchVideoをfalseにさせてもらうぜ…！！
            //SceneLoad.WatchVideo = false;
            Debug.Log("現在は" + SceneLoad.WatchVideo + "だぜ……！");

            RewardUI.gameObject.SetActive(true);


            SceneLoad.WatchVideo = false;

            GLS.GLSAnalyticsUtility.TrackEvent("Stage residence time report", "Stage residence time", OnStageTime);
            SceneLoad.ClearedStage();
        }

    }


    void Awake()
    {
        DateCountScript.AddLogInDays();
    }

}
