﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameDirector : MonoBehaviour
{
    GameObject Wash;
    GameObject Amount;
    GameObject RewardUI;

    int wash = 0;
    float OnStageTime = 0;


    public static int thankyou = 0;

    //一度だけ送信したいので
    private bool oneTime60sec = true;
    private bool oneTime180sec = true;
    private bool oneTime300sec = true;

    //洗う数
    public int WashAmount;

    public float X;
    public float Y;
    public float Z;

    public GameObject []Target;

    public List<Vector3> SpawnPoint = new List<Vector3>();
    private List<GameObject> objList = new List<GameObject>();
    private List<int> randomList = new List<int>();
    private int count = 0;



    void Start()
    {
        thankyou = 0;

        //洗った数のテキスト
        Wash = GameObject.Find("WashText");

        //ゲームクリア時のUI
        RewardUI = GameObject.Find("RewardUI");

        RewardUI.gameObject.SetActive(false);

        //SceneLoad.WatchVideo = false;

        //洗う数のランダム化
        WashAmount = Random.Range(3, 9);            //3～9体のおっさんを生み出す
        Amount = GameObject.Find("AmountText");

        //きれいにする人の数
        Amount.GetComponent<Text>().text = WashAmount.ToString();



        //ランダムな座標におっさんを生み出す
        //条件を満たすまでループ
        while (true)
        {
            int i = Random.Range(0, 9); //ランダム生成
            int p = Random.Range(0, Target.Length);
            //ランダムで出た数字が既に出た数字と被っていたら再抽選
            if (randomList.Contains(i))
            {
                continue;               //無視してもう一度
            }
            
            //上手くいったら
            count++;                    //作った数+1
            //Y軸で180回転させて生成
            GameObject obj = Instantiate(Target[p], SpawnPoint[i], Quaternion.AngleAxis(180f, new Vector3(0.0f, 1.0f, 0.0f)));
            obj.transform.localScale = new Vector3(X,Y,Z);//生成するときのサイズ変更
            objList.Add(obj);           //作ったインスタンスを保持
            randomList.Add(i);          //ランダムで出てきた数字を保持


            //作りたい数を満たしたら
            if (count >= WashAmount)
            {
                break;                  //ループを抜ける
            }
        }
    }





    private void Update()
    {
        OnStageTime += Time.deltaTime;

        //ここに起動してからの時間が蓄積されていく
        DateCountScript.stayTime += Time.deltaTime;         

        
        //一定時間に達したら一度だけカスタムイベントを送信
        if (DateCountScript.stayTime > 60f)
        {
            if (oneTime60sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("SendStayTimeReport", "StayTime60sec", DateCountScript.stayTime);
                oneTime60sec = false;
            }
        }
        if (DateCountScript.stayTime > 180f)
        {
            if (oneTime180sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("SendStayTimeReport", "StayTime180sec", DateCountScript.stayTime);
                oneTime180sec = false;
            }
        }
        if (DateCountScript.stayTime > 300f)
        {
            if (oneTime300sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("SendStayTimeReport", "StayTime300sec", DateCountScript.stayTime);
                oneTime300sec = false;
            }
        }
    }
    
    public void Clean()
    {
        wash++;
        Wash.GetComponent<Text>().text = wash.ToString(); //きれいにした人数の表示
    }

    public void Thankyou()
    {
        thankyou++;
        Debug.Log(thankyou);

    

        //全員きれいにしたら
        if (thankyou== WashAmount)
        {
            //SceneLoadのWatchVideoをfalseにさせてもらうぜ…！！
            //SceneLoad.WatchVideo = false;
            Debug.Log("現在は"+SceneLoad.WatchVideo+"だぜ……！");

            RewardUI.gameObject.SetActive(true);


            SceneLoad.WatchVideo = false;

            GLS.GLSAnalyticsUtility.TrackEvent("Stage residence time report", "Stage residence time", OnStageTime);
            SceneLoad.ClearedStage();
        }

    }


    void Awake()
    {
        DateCountScript.AddLogInDays();
    }

   
}
