﻿using UnityEngine;
using UnityEngine.UI;
public class AdBannerSampleScript : MonoBehaviour
{
    [SerializeField]
    private Button showBannerButton = null;
    [SerializeField]
    private Button hideBannerButton = null;
    private void Start()
    {
        // ◆ShowBannerボタンが押された時の処理
        showBannerButton.onClick.AddListener(() =>
        {
            // ◆バナー広告を表示
            GLS.Ad.ShowBanner();
        });
        // ◆HideBannerボタンが押された時の処理
        hideBannerButton.onClick.AddListener(() =>
        {
            // ◆バナー広告を非表示
            GLS.Ad.HideBanner();
        });
    }
}