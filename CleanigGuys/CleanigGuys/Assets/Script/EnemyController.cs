﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    // public static int thankyou = 0;
    //public float x_plus;
    //public float x_minus;
    //public float z_plus;

    public EnemyGenerator enemyGenerator = null;

    void Start()
    {
        // thankyou = 0;
    }

    public void Move()
    {
        if (transform.position.x <= 0)
        {
            transform.Translate(-0.007f, 0, 0.05f); // -0.007f, 0, 0.05f
        }

        else
        {
            transform.Translate(0.007f, 0, 0.05f);  // 0.007f, 0, 0.05f
        }
    }


    public void OutRange()
    {
        if (transform.position.z < -2.12f)
        {
            Destroy(gameObject);
            enemyGenerator.Enemycount--;
        }
    }


    public void Update()
    {

        Move();
        OutRange();
      
    }

  
}
