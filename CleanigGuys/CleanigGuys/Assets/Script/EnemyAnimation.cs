﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    // Animatorを参照するための準備
    [SerializeField]
    private Animator animator;

    private float spendTime = 0;                //経過時間
    private float randomTime;

    float ResetX;
    float ResetY;
    float ResetZ;

    private void Start()
    {
        randomTime = Random.Range(3f, 11f);    //アニメーション切り替えのタイミングはランダムで指定する
        ResetX = transform.position.x;
        ResetY = transform.position.y;
        ResetZ = transform.position.z;

    }

    private void Update()
    {
        spendTime += Time.deltaTime;
        if(spendTime > randomTime)
        {
            int number = Random.Range(1, 3);    //どのアニメーションを再生するか(ランダム)
            AnimationChanger(number);           //引数で指定したアニメーションを再生
            randomTime += 11f;                  //アニメーションの再生時間を考慮して次に再生されるまでの時間を長くする
            spendTime = 0;
   
        }
    }

    public void ThankyouAnimation()
    {
        //Animatorで設定したthanksをF→Tにする
        animator.SetBool("thanks", true);
    }


    //ランダムなアニメーションを再生する
    private void AnimationChanger(int value)
    {
        animator.SetInteger("StartAnimation", value);
      
    }
}
