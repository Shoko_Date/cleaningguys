﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCount1112 : MonoBehaviour
{
    public int Hit;
    public Material[] _material;
    public GameObject Kira;
    public GameObject effect;


    //private void Destroy()
    //{
    //    // 呼び出されたらこのオブジェクトを消すメソッド
    //    Destroy(gameObject);    
    //}


    private void Start()
    {
        GameObject Tag = GameObject.FindGameObjectWithTag("Human");
    }
    public void OnParticleCollision(GameObject Collision)
    {
        Hit++;
        effect.GetComponent<ParticleSystem>().Play();
        //Debug.Log("当たってる");

        if (SceneLoad.WatchVideo == true)
        {
            if (Hit >= 10)
            {
                //マテリアル1を割り当てる
                GetComponent<Renderer>().sharedMaterial = _material[1];
            }
            if (Hit >= 20)
            {
                // 一番上の親オブジェクトを取得
                GameObject enemy = transform.root.gameObject;
                //if (Collision.gameObject.tag == "Human")
                //{
                //Thankyouというアニメーションを呼び出すメソッドを召喚
                enemy.GetComponent<EnemyAnimation>().ThankyouAnimation();
                // }


                // Colliderを無効にする
                GetComponent<BoxCollider>().enabled = false;

                // マテリアル2を割り当てる
                GetComponent<Renderer>().sharedMaterial = _material[2];

                // Kirakiraを再生する
                // star.Play();
                Instantiate(Kira, this.transform.position, Quaternion.identity); //パーティクル用ゲームオブジェクト生成

                //Hit数を表示しておく
                Debug.Log(Hit);

                // ThankyouとCleanを読み込む
                GameObject director = GameObject.Find("GameDirector");
                director.GetComponent<GameDirectorStage1112>().Thankyou();
                director.GetComponent<GameDirectorStage1112>().Clean();
            }
        }
        else
        {
            if (Hit >= 40)
            {
                //マテリアル1を割り当てる
                GetComponent<Renderer>().sharedMaterial = _material[1];
            }


            if (Hit >= 80)
            {
                // 一番上の親オブジェクトを取得
                GameObject enemy = transform.root.gameObject;
                //if (Collision.gameObject.tag == "Human")
                //{
                //Thankyouというアニメーションを呼び出すメソッドを召喚
                enemy.GetComponent<EnemyAnimation>().ThankyouAnimation();
                // }


                // Colliderを無効にする
                GetComponent<BoxCollider>().enabled = false;

                // マテリアル2を割り当てる
                GetComponent<Renderer>().sharedMaterial = _material[2];

                // Kirakiraを再生する
                // star.Play();
                Instantiate(Kira, this.transform.position, Quaternion.identity); //パーティクル用ゲームオブジェクト生成

                //Hit数を表示しておく
                Debug.Log(Hit);

                // ThankyouとCleanを読み込む
                GameObject director = GameObject.Find("GameDirector");
                director.GetComponent<GameDirectorStage1112>().Thankyou();
                director.GetComponent<GameDirectorStage1112>().Clean();

            }

        }


    }
}

