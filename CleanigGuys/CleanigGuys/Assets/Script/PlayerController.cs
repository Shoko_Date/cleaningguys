using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("縦軸回転の制限")]
    public float rotateLimitX = 30.0f;
    [Header("横軸回転の制限")]
    public float rotateLimitY = 30.0f;
    [Header("回転速度")]
    public  float rotateSpeed = 1.0f;
    [Header("補間機能のスピード")]
    public float smoothSpeed = 10.0f;

    private Vector3 firstTapPosition = Vector3.zero;
    private Vector3 secondTapPosition = Vector3.zero;
    private Vector3 rotateAngle = Vector3.zero;
    private Vector2 smoothVelocity = Vector2.zero;

    private void Start()
    {
        // ◆バナー広告を表示
        GLS.Ad.ShowBanner();

        // 開始時に今のカメラの角度を取得
        rotateAngle = transform.rotation.eulerAngles;
    }

    void Update()
    {
        // マウスをクリックした時
        if (Input.GetMouseButtonDown(0))
        {
            // クリック開始地点の座標
            firstTapPosition = Input.mousePosition;
        }

        // マウスをクリックし続けた時
        if (Input.GetMouseButton(0))
        {
            // マウスを移動させた地点の座標
            secondTapPosition = Input.mousePosition;

            // カメラの回転処理
            CameraRotate();
        }
    }

    private void CameraRotate()
    {
        if (firstTapPosition != secondTapPosition)
        {
                  // マウスの移動方向の取得
            Vector3 direction = secondTapPosition - firstTapPosition;

            // y軸の移動方向反転
            direction.y *= -1;

            // ベクトルの正規化
            direction.Normalize();

            // 回転量の計算
            Vector3 rotate = new Vector3(direction.y, direction.x, 0) * rotateSpeed * Time.deltaTime;

            // 回転量を足す
            rotateAngle += rotate;

            // 縦軸回転の角度制限
            if (rotateAngle.x > rotateLimitX)
                rotateAngle.x = rotateLimitX;
            if (rotateAngle.x < -rotateLimitX)
                rotateAngle.x = -rotateLimitX;

            // 横軸回転の角度制限
            if (rotateAngle.y > rotateLimitY)
                rotateAngle.y = rotateLimitY;
            if (rotateAngle.y < -rotateLimitY)
                rotateAngle.y = -rotateLimitY;
        }

        // 回転の反映
        var smoothRotateX = Mathf.SmoothDampAngle(transform.eulerAngles.x, rotateAngle.x, ref smoothVelocity.x, smoothSpeed * Time.deltaTime);
        var smoothRotateY = Mathf.SmoothDampAngle(transform.eulerAngles.y, rotateAngle.y, ref smoothVelocity.y, smoothSpeed * Time.deltaTime);
        gameObject.transform.eulerAngles = new Vector3(smoothRotateX, smoothRotateY, 0.0f);

        firstTapPosition = secondTapPosition;
    }

}
