﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hose : MonoBehaviour
{
    public AudioClip[] sound;
    AudioSource audioSource;

    public Material[] Mat;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        Debug.Log(SceneLoad.WatchVideo);

        //Debug.Log("このシーン中は"+SceneLoad.WatchVideo+"です！");
        GetComponent<ParticleSystemRenderer>().material = Mat[0];

        if (SceneLoad.WatchVideo == true)
        {
            //水の色変更
            GetComponent<ParticleSystemRenderer>().material = Mat[1];
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            audioSource.volume = 1;
            if(SceneLoad.WatchVideo==true)
            {
                audioSource.PlayOneShot(sound[1]);
            }
            else
            {
                audioSource.PlayOneShot(sound[0]);
            }
            GetComponent<ParticleSystem>().Play();
        }

        if (Input.GetMouseButtonUp(0))
        {
            GetComponent<ParticleSystem>().Stop();
            audioSource.Stop();
        }

    }

     public void SoundStop()
    {
        audioSource.Stop();
    }
}
