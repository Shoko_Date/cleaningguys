﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    public GameObject EnemyA;
    public GameObject EnemyB;
    public GameObject EnemyC;

    float span = 0.5f;
    float delta = 0;
    public int Enemycount = 0;

    public GameObject UI;

    bool ks = false;

    void Start()
    {
        
    }

    private List<GameObject> myList = new List<GameObject>();



    // Update is called once per frame
    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ks = true;
        }


        if(ks==true)
        {

            Destroy(UI);

            // List内の数が5個以下の時
            if (Enemycount <= 5)
            {
                int tate = Random.Range(20, 25);    // 60~70
                int yoko = Random.Range(-4, 5);   // -12~13

                delta += Time.deltaTime;

                if (delta > span)
                {
                    delta = 0;
                    GameObject go = Instantiate(EnemyA) as GameObject; //エネミーを生成
                    go.transform.position = new Vector3(yoko, 0.5f, tate);　//生成する場所

                    EnemyController enemyController = go.GetComponent<EnemyController>();　//E
                    enemyController.enemyGenerator = this;

                    // Enemycountをプラス
                    Enemycount++;
                }

                delta += Time.deltaTime;
                if (delta > span)
                {
                    delta = 0;
                    GameObject go = Instantiate(EnemyB) as GameObject;
                    go.transform.position = new Vector3(yoko, 0.5f, tate);

                    EnemyController enemyController = go.GetComponent<EnemyController>();
                    enemyController.enemyGenerator = this;
                    // EnemyCountをプラス
                    Enemycount++;
                }

                delta += Time.deltaTime;
                if (delta > span)
                {
                    delta = 0;
                    GameObject go = Instantiate(EnemyC) as GameObject;
                    go.transform.position = new Vector3(yoko, 0.5f, tate);

                    EnemyController enemyController = go.GetComponent<EnemyController>();
                    enemyController.enemyGenerator = this;
                    // Enemycountをプラス
                    Enemycount++;

                }
            }
            
        }
    }

    
}
